import pandas as pd
import numpy as np


def load_data(file_name):
    X = np.array(pd.read_csv(file_name))  # load file and convert to array
    return X

def split_data(X, y, tr_fraction=0.5):
    """Split the data X,y into two random subsets."""
    np.random.shuffle(X)  # shuffle X's rows

    numb_of_elements = np.shape(X)[0]
    Xtr = X[:int(numb_of_elements * tr_fraction), :]
    ytr = y[:int(numb_of_elements * tr_fraction)]
    Xts = X[int(numb_of_elements * tr_fraction):, :]
    yts = y[int(numb_of_elements * tr_fraction):]

    return Xtr, ytr, Xts, yts


def count_digits(y):
    """Count the number of elements in each class."""

    s = []
    i = 0
    for x in np.unique(y):
        s.append(0)
        for n in y:
            if x == n:
                s[i] += 1
        i += 1

    s = np.array(s)
    s.dtype = int
    return s


def fit(Xtr, ytr):
    """Compute the average centroid for each class."""
    template_list = []
    temporary_list = []
    i = 0
    for number in np.unique(ytr):
        for j in range(np.size(ytr)):
            if ytr[j] == number:
                temporary_list.append(Xtr[j, :])  # add the array in number list

        mid = sum(temporary_list) / count_digits(ytr)[i]
        temporary_list = []
        template_list.append(mid)
        i += 1
    template_list = np.array(template_list)
    return template_list

# xfile_name = 'mnist_data.csv'

# X = load_data('data\mnist_data.csv')
